<div class="postslist">
<div class="left"><h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2> / <?php the_author(); ?> / <?php the_time('F jS, Y'); ?>, <?php the_time('g:i a'); ?></div>
<div class="right"><?php echo $post->comment_count?><?php echo ($post->comment_count==1?' comment':' comments');?></div>
</div>